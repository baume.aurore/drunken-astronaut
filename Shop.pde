class Shop {
  
   ArrayList<Article> articles;
   boolean show;
   
   Shop() { //<>//
     PImage bananeimg = loadImage("data/beer.png");
     Article banane = new Article ("BANANE", 5, bananeimg, "Indispensable à votre survie, si vous ne voulez pas vivre que de bière.");
     
     PImage teslaimg = loadImage("data/beer.png");
     Article tesla = new Article ("TESLA", 50, teslaimg, "Pour faire vroum vroum sans un bruit.");
     
     articles = new ArrayList<Article>();
     articles.add(banane);
     articles.add(tesla);
     
     show = false;
   }
   
   void display() {
     if (keyPressed && key == 'm' && !show) {       
       show = true;
     } else if (keyPressed && key == 'm' && show){
       show = false;
     }
     
     if (show) {
       show();
     }
   }
   
   void show() {
     fill(100);
     rect(width/4, height/4, width/2, height/2);
     
     float posx = width/4 + 2;
     float posy = height/4 + 2;
     for (Article a : articles) {
       a.setXPos(posx);
       a.setYPos(posy);
       a.display();
       a.mouseClicked();
       
       posy = posy + 30;
     }
   }
  
}
