class Player {

  String name;
  PImage img;
  float xpos;
  float ypos;
  float drunken_rate;
  float speed;
  int nb_beers;
  float money;

  Player() {
    name = "Théo";
    img = loadImage("data/astronaut.png");
    xpos = 0;
    ypos = 0;
    speed = 3;
    drunken_rate = 10;
    nb_beers = 40;
    money = 0.0;
  }

  void display() {
    image(img, xpos, ypos, 60, 60);
    showDrunkenRate();
    showNbBeers();
    showMoney();
  }

  void move() {
    if (drunken_rate == 100) {
      if (keyPressed == true) {
        switch (key) {
        case 'z': 
          ypos = ypos + 1*speed; 
          break;
        case 's': 
          ypos = ypos - 1*speed; 
          break;
        case 'q': 
          xpos = xpos + 1*speed; 
          break;
        case 'd': 
          xpos = xpos - 1*speed; 
          break;
        default: 
          break;
        }
      }
    } else if (keyPressed == true) {
      switch (key) {
      case 'z': 
        ypos = ypos - 1*speed; 
        break;
      case 's': 
        ypos = ypos + 1*speed; 
        break;
      case 'q': 
        xpos = xpos - 1*speed; 
        break;
      case 'd': 
        xpos = xpos + 1*speed; 
        break;
      default: 
        break;
      }
    }
  }
  
  void catchBeer() {
    nb_beers++;
  }

  void showDrunkenRate() {
    int bar_height = 20;
    int bar_width = 100;
    color color_bar = color(204, 102, 0);

    if (drunken_rate < 30) {
      color_bar = color(0, 255, 0);
    }

    if (drunken_rate > 90) {
      color_bar = color(255, 0, 0);
    }

    fill(100);
    rect(width - bar_width - 10, height - bar_height - 10, bar_width, bar_height, 7);
    fill(color_bar);
    rect(width- bar_width - 9, height - bar_height - 9, (bar_width - 2) * drunken_rate / 100, bar_height - 2, 7);
  }
  
  PImage img_beer = loadImage("data/beer.png"); 

  void showNbBeers() {
    image(img_beer, width - 60, height - 85, 30, 30);
    textSize(32);
    text(nb_beers, width - 100, height - 60);
  }
  
  void showMoney() {
    textSize(12);
    String text_money = String.valueOf(money) + '€';
    text(text_money, width - 120, height - 100); //<>//
  }
  
  float getXPos() {
   return xpos; 
  }
  
  float getYPos() {
   return ypos; 
  }
  
  float getMoney() {
    return money;
  }
  
  void setMoney(float nmoney) {
    money = nmoney;
  }
}
