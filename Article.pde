class Article {

  String name;
  float price;
  PImage picture;
  String description;
  float xpos;
  float ypos;
  boolean purchased;

  Article(String nname, float nprice, PImage npicture, String ndescription) {
    name = nname;
    price = nprice;
    picture = npicture;
    description = ndescription;
    purchased = false;
  }

  void display() {
    image(picture, xpos, ypos, 30, 30);
    if (purchased) {
      fill(150);
    } else {
      fill(255);
    }
    textSize(15);
    String text = name + " : " + description + " " + price + "€";
    text(text, xpos + 30, ypos + 20);
  }

  void mouseClicked() {
    if (mousePressed && mouseX > xpos && mouseX < (xpos + width / 2) && mouseY > ypos && mouseY < (ypos + 30)) {
      purchased = true;
    }
  }

  void setXPos(float pos) {
    xpos = pos;
  }

  void setYPos(float pos) {
    ypos = pos;
  }
}
