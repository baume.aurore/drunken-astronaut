class Beer {
  PImage img;
  float xpos;
  float ypos;
  boolean catched;

  Beer(float new_xpos, float new_ypos) {
    img = loadImage("data/beer.png"); 
    xpos = new_xpos;
    ypos = new_ypos;
    catched = false;
  }

  void display() {
    if (catched == false) {
      image(img, xpos, ypos, 50, 50);
      collisionWithPlayer();
    }
  }

  void collisionWithPlayer() {
    float left_side = xpos;
    float right_side = xpos + 50;
    float top_side = ypos;
    float bottom_side = ypos + 50;
    if ((player.getXPos() > left_side && player.getXPos() < right_side) && (player.getYPos() > top_side && player.getYPos() < bottom_side)) {
      player.catchBeer();
      catched = true;
    }
  }
}
