Player player; 
Shop shop;

void setup() {
  size(1400, 800);
  background(255);
  
  player = new Player();
  player.display();
  
  shop = new Shop();
 
  showPlanet();
  
}

void draw() {
  background(255);
  player.move();
  player.display();
  heineken.display();
  
  shop.display();
}
