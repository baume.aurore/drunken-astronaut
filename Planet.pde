import java.util.Random;

class Planet {
  String name;
  float xpos;
  float ypos;
  PImage img;
  ArrayList<Beer> beers;

  Planet(String new_name, float new_xpos, float new_ypos, float nb_beer) {
    name = new_name;
    xpos = new_xpos;
    ypos = new_ypos;
    img = loadImage("data/planet.png");
    
    beers = new ArrayList<Beer>();
    for (int i=0; i<nb_beer; i++) {
      beers.add(beerWithPosition());
    }
  }

  void display() {
    image(img, xpos, ypos, 200, 200);
    for (Beer b : beers) {
      b.display();
    }
  }

  Beer beerWithPosition() {
    Random rnd = new Random();
    noStroke();
    fill(255, 0, 0);

    int r = rnd.nextInt(360);
    int rayon = 100;

    float x = (float)(xpos + 80 + ((rayon + 40) * Math.cos(r)));
    float y = (float)(ypos + 80 + ((rayon + 40) * Math.sin(r)));

    return new Beer(x, y); //<>//
  }
}

Planet heineken;

void showPlanet() {
  heineken = new Planet("heineken", 700, 60, 2);
  heineken.display();
}
